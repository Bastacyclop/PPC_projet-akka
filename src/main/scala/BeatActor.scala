package upmc.akka.leader

import math._

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor._

sealed trait BeatMessage
case class BeatTick () extends Tick

class BeatActor (val id: Int) extends Actor {
  val period = FiniteDuration(50L, MILLISECONDS)
  val father = context.parent

  def receive = {
    case Start => {
      self ! BeatTick
    }

    case BeatTick => {
      father ! BeatTick
      context.system.scheduler.scheduleOnce(period, self, BeatTick)
    }
  }
}
