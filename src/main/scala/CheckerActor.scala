package upmc.akka.leader

import java.util
import java.util.Date

import akka.actor._

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

abstract class Tick
case class CheckerTick () extends Tick

class CheckerActor (
  val id: Int,
  val electionActor: ActorRef,
  val displayActor: ActorRef
) extends Actor
{
  val delta_ms = 200L
  val period = FiniteDuration(delta_ms, MILLISECONDS)
  val father = context.parent

  var leader = -1
  var lastBeats = scala.collection.mutable.Map[Int, Long]()

  def receive = {
    case Start => {
      self ! CheckerTick
    }

    case IsAlive(nodeId) => {
      if (!(lastBeats contains nodeId)) {
        displayActor ! Message(nodeId + " is alive")
      }
      lastBeats(nodeId) = System.currentTimeMillis()
    }

    case LeaderChanged(nodeId) => {
      leader = nodeId
    }

    case CheckerTick => {
      val now = System.currentTimeMillis()
      lastBeats = lastBeats.filter({ case (id, last) => {
        val awol = (now - last) > delta_ms
        if (awol) {
          displayActor ! Message(id + " is AWOL")
          if (id == leader) {
            electionActor ! Initiate
          }
        }
        !awol
      }})
      context.system.scheduler.scheduleOnce(period, self, CheckerTick)
    }
  }
}
