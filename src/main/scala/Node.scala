package upmc.akka.leader

import akka.actor._

case class Start ()

sealed trait SyncMessage
case class Sync (nodes:List[Int]) extends SyncMessage
case class SyncForOneNode (nodeId:Int, nodes:List[Int]) extends SyncMessage

sealed trait AliveMessage
case class IsAlive (id:Int) extends AliveMessage
case class IsAliveLeader (id:Int) extends AliveMessage

case class LeaderChanged (id: Int)

class Node (val id:Int, val terminaux:List[Terminal])
    extends Actor
{
  val displayActor = context.actorOf(Props[DisplayActor], name = "displayActor")
  val electionActor = context.actorOf(Props(new ElectionActor(this.id, terminaux)), name = "electionActor")
  val checkerActor = context.actorOf(Props(new CheckerActor(this.id, electionActor, displayActor)), name = "checkerActor")
  val beatActor = context.actorOf(Props(new BeatActor(this.id)), name = "beatActor")

  var allNodes:List[ActorSelection] = List()

  def receive = {
    case Start => {
      displayActor ! Message ("Node " + this.id + " is created")
      checkerActor ! Start
      beatActor ! Start

      terminaux.foreach(n => {
        if (n.id != id) {
          val remote = context.actorSelection("akka.tcp://LeaderSystem" + n.id + "@" + n.ip + ":" + n.port + "/user/Node")
          // Mise a jour de la liste des nodes
          this.allNodes = this.allNodes:::List(remote)
        }
      })
    }

    case BeatTick => {
      allNodes.foreach(n => n ! IsAlive(id))
    }

    case IsAlive(id) => {
      checkerActor ! IsAlive(id)
    }

    case LeaderChanged(nodeId) => {
      checkerActor ! LeaderChanged(nodeId)
    }
  }
}
